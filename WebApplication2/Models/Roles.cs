﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication2.Models
{
    public class Roles
    {
        //1= admin, 2 =Writer
        [Key]
        public int IdRoles { get; set; }
        public string Name { get; set; }
        public ICollection<Users> GetUsers {get;set;}
    }
}
