﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication2.Models
{
    public class Authores_has_Books
    {
        [Key]
        public int IdAuthores_has_Books { get; set; }

        //Foreing Key
        public int IdBook { get; set; }
        public int IdUserAuthor { get; set; }
        public int IdUserCreate { get; set; }
        public Users users { get; set; }
        public Books books { get; set; }

    }
}
