﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication2.Models
{
    public class Users
    {
        [Key]
        public int IdUser { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public DateTime? BirthDay { get; set; }
        public string DocumentNumber { get; set; }
        public int Status { get; set; }

        //Foreign Key
        public int IdRoles { get; set; }
        public Roles roles { get; set; }

        public ICollection<Books> GetBooks {get;set;}
        public ICollection<Authores_has_Books> GetAuthores_Has_Books { get; set; }

    }

}
