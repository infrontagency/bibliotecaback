﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication2.Models.Transaction
{
    public class GetBooks
    {
        public int IdBooks { get; set; }
        public string Name { get; set; }
        public string synapse { get; set; }
        public int NumberPages { get; set; }
        public DateTime PublicationDate { get; set; }
        public string NameEditorial { get; set; }
        public string NameGenre { get; set; }

    }
}
