﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication2.Models
{
    public class Books
    {
        [Key]
        public int IdBooks { get; set; }
        public string Name { get; set; }
        public string synapse { get; set; }
        public int NumberPages { get; set; }
        public DateTime PublicationDate { get; set; }

        //Foreing Key
        public ICollection<Authores_has_Books> GetAuthores_Has_Books { get; set; }

        public int IdBookGenre { get; set; }
        public BookGenre bookGenre { get; set; }

        public int IdEditorials { get; set; }
        public Editorials editorials { get; set; }


    }
}
