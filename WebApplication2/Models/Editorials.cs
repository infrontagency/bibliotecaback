﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication2.Models
{
    public class Editorials
    {
        [Key]
        public int IdEditorials { get; set; }
        public string Name { get; set; }
        public string Sede { get; set; }
        public ICollection<Books> GetBooks { get; set; }
    }
}
