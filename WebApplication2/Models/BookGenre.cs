﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication2.Models
{
    public class BookGenre
    {
       [Key]
        public int IdBookGenre { get; set; }
        public string Name { get; set; }


        //Foreing Key/

        public ICollection<Books> GetBooks { get; set; }
    }
}
