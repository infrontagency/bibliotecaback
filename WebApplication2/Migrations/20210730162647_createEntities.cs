﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApplication2.Migrations
{
    public partial class createEntities : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "BirthDay",
                table: "Users",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DocumentNumber",
                table: "Users",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Email",
                table: "Users",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "IdRoles",
                table: "Users",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "LastName",
                table: "Users",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "Users",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Password",
                table: "Users",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Phone",
                table: "Users",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "bookGenres",
                columns: table => new
                {
                    IdBookGenre = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_bookGenres", x => x.IdBookGenre);
                });

            migrationBuilder.CreateTable(
                name: "Editorials",
                columns: table => new
                {
                    IdEditorials = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    Sede = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Editorials", x => x.IdEditorials);
                });

            migrationBuilder.CreateTable(
                name: "roles",
                columns: table => new
                {
                    IdRoles = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_roles", x => x.IdRoles);
                });

            migrationBuilder.CreateTable(
                name: "books",
                columns: table => new
                {
                    IdBooks = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    synapse = table.Column<string>(nullable: true),
                    NumberPages = table.Column<int>(nullable: false),
                    PublicationDate = table.Column<DateTime>(nullable: false),
                    IdBookGenre = table.Column<int>(nullable: false),
                    IdEditorials = table.Column<int>(nullable: false),
                    UsersIdUser = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_books", x => x.IdBooks);
                    table.ForeignKey(
                        name: "FK_books_bookGenres_IdBookGenre",
                        column: x => x.IdBookGenre,
                        principalTable: "bookGenres",
                        principalColumn: "IdBookGenre");
                    table.ForeignKey(
                        name: "FK_books_Editorials_IdEditorials",
                        column: x => x.IdEditorials,
                        principalTable: "Editorials",
                        principalColumn: "IdEditorials");
                    table.ForeignKey(
                        name: "FK_books_Users_UsersIdUser",
                        column: x => x.UsersIdUser,
                        principalTable: "Users",
                        principalColumn: "IdUser",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Authores_has_Books",
                columns: table => new
                {
                    IdAuthores_has_Books = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IdBook = table.Column<int>(nullable: false),
                    IdUserAuthor = table.Column<int>(nullable: false),
                    IdUserCreate = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Authores_has_Books", x => x.IdAuthores_has_Books);
                    table.ForeignKey(
                        name: "FK_Authores_has_Books_books_IdBook",
                        column: x => x.IdBook,
                        principalTable: "books",
                        principalColumn: "IdBooks");
                    table.ForeignKey(
                        name: "FK_Authores_has_Books_Users_IdUserCreate",
                        column: x => x.IdUserCreate,
                        principalTable: "Users",
                        principalColumn: "IdUser");
                });

            migrationBuilder.CreateIndex(
                name: "IX_Users_IdRoles",
                table: "Users",
                column: "IdRoles");

            migrationBuilder.CreateIndex(
                name: "IX_Authores_has_Books_IdBook",
                table: "Authores_has_Books",
                column: "IdBook");

            migrationBuilder.CreateIndex(
                name: "IX_Authores_has_Books_IdUserCreate",
                table: "Authores_has_Books",
                column: "IdUserCreate");

            migrationBuilder.CreateIndex(
                name: "IX_books_IdBookGenre",
                table: "books",
                column: "IdBookGenre");

            migrationBuilder.CreateIndex(
                name: "IX_books_IdEditorials",
                table: "books",
                column: "IdEditorials");

            migrationBuilder.CreateIndex(
                name: "IX_books_UsersIdUser",
                table: "books",
                column: "UsersIdUser");

            migrationBuilder.AddForeignKey(
                name: "FK_Users_roles_IdRoles",
                table: "Users",
                column: "IdRoles",
                principalTable: "roles",
                principalColumn: "IdRoles");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Users_roles_IdRoles",
                table: "Users");

            migrationBuilder.DropTable(
                name: "Authores_has_Books");

            migrationBuilder.DropTable(
                name: "roles");

            migrationBuilder.DropTable(
                name: "books");

            migrationBuilder.DropTable(
                name: "bookGenres");

            migrationBuilder.DropTable(
                name: "Editorials");

            migrationBuilder.DropIndex(
                name: "IX_Users_IdRoles",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "BirthDay",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "DocumentNumber",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "Email",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "IdRoles",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "LastName",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "Password",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "Phone",
                table: "Users");
        }
    }
}
