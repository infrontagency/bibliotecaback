﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WebApplication2.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Microsoft.AspNetCore.Http;
using System.Net;
using System.Net.Http;

namespace WebApplication2
{
    public class Startup
    {
        readonly string MyAllowSpecificOrigins = "_myAllowSpecificOrigins";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();
            services.AddControllers();
            services.AddDbContext<WebApplication2Context>(options =>
                  options.UseSqlServer(Configuration.GetConnectionString("DevConnection")));
           
            services.AddAuthentication(opt => {
                opt.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                opt.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                }).AddJwtBearer(options=>{

                    options.TokenValidationParameters = new Microsoft.IdentityModel.Tokens.TokenValidationParameters {
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,
                        ValidIssuer = Configuration["ApiAuth:Issuer"],
                        ValidAudience = Configuration["ApiAuth:Audience"],

                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["ApiAuth:SecretKey"]))

                    };                 
                });

          
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseCors(options=> {
                options.WithOrigins("*");
                options.AllowAnyHeader();
                options.AllowAnyMethod();
            });

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseMiddleware<AdminSafeListMiddleware>(Configuration["AdminSafeList"]);

            app.UseHttpsRedirection();

            app.UseRouting();
            app.UseAuthorization();
            app.UseAuthentication();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        public class AdminSafeListMiddleware
        {
            private readonly RequestDelegate _next;
            private readonly ILogger<AdminSafeListMiddleware> _logger;
            private readonly byte[][] _safelist;

            public AdminSafeListMiddleware(
                RequestDelegate next,
                ILogger<AdminSafeListMiddleware> logger,
                string safelist)
            {
                var ips = safelist.Split(';');
                _safelist = new byte[ips.Length][];
                for (var i = 0; i < ips.Length; i++)
                {
                    _safelist[i] = IPAddress.Parse(ips[i]).GetAddressBytes();
                }

                _next = next;
                _logger = logger;
            }

            public async Task Invoke(HttpContext context)
            {
                if (context.Request.Method != HttpMethod.Get.Method)
                {
                    var remoteIp = context.Connection.RemoteIpAddress;
                    _logger.LogDebug("Request from Remote IP address: {RemoteIp}", remoteIp);

                    var bytes = remoteIp.GetAddressBytes();
                    var badIp = true;
                    foreach (var address in _safelist)
                    {
                        if (address.SequenceEqual(bytes))
                        {
                            badIp = false;
                            break;
                        }
                    }

                    if (badIp)
                    {
                        _logger.LogWarning(
                            "Forbidden Request from Remote IP address: {RemoteIp}", remoteIp);
                        context.Response.StatusCode = (int)HttpStatusCode.Forbidden;
                        return;
                    }
                }

                await _next.Invoke(context);
            }
        }
    }
}
