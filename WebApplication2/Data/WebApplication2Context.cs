﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WebApplication2.Models;

namespace WebApplication2.Data
{
    public class WebApplication2Context : DbContext
    {
        public WebApplication2Context (DbContextOptions<WebApplication2Context> options)
            : base(options)
        {
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //Relation Users
            modelBuilder.Entity<Users>().HasOne(x => x.roles).WithMany(c => c.GetUsers).HasForeignKey(x => x.IdRoles).OnDelete(DeleteBehavior.NoAction);

            //books
            modelBuilder.Entity<Users>().HasOne(x => x.roles).WithMany(c => c.GetUsers).HasForeignKey(x => x.IdRoles).OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<Authores_has_Books>().HasOne(x => x.users).WithMany(c => c.GetAuthores_Has_Books).HasForeignKey(x => x.IdUserAuthor).OnDelete(DeleteBehavior.NoAction);
            modelBuilder.Entity<Authores_has_Books>().HasOne(x => x.users).WithMany(c => c.GetAuthores_Has_Books).HasForeignKey(x => x.IdUserCreate).OnDelete(DeleteBehavior.NoAction);
            modelBuilder.Entity<Authores_has_Books>().HasOne(x => x.books).WithMany(c => c.GetAuthores_Has_Books).HasForeignKey(x => x.IdBook).OnDelete(DeleteBehavior.NoAction);
            
            modelBuilder.Entity<Books>().HasOne(x => x.bookGenre).WithMany(c => c.GetBooks).HasForeignKey(x => x.IdBookGenre).OnDelete(DeleteBehavior.NoAction);
            modelBuilder.Entity<Books>().HasOne(x => x.editorials).WithMany(c => c.GetBooks).HasForeignKey(x => x.IdEditorials).OnDelete(DeleteBehavior.NoAction);



            //roles

            //booksgenre
        }
        public DbSet<WebApplication2.Models.Users> Users { get; set; }
        public DbSet<WebApplication2.Models.Books> books { get; set; }
        public DbSet<WebApplication2.Models.Roles> roles { get; set; }
        public DbSet<WebApplication2.Models.BookGenre> bookGenres { get; set; }
        public DbSet<WebApplication2.Models.Editorials> editorials { get; set; }
    }
}
