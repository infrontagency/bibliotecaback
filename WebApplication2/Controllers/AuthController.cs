﻿using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using WebApplication2.Data;
using WebApplication2.Models;
using System.Text.RegularExpressions;
using BC = BCrypt.Net.BCrypt;
using WebApplication2.Models.Transaction;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;

namespace WebApplication2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private WebApplication2Context db;
        IHostingEnvironment _env;
        private readonly IConfiguration _configuration;
        private readonly ILogger<AuthController> _logger;
        public AuthController(Data.WebApplication2Context context, IHostingEnvironment environment, IConfiguration configuration, ILogger<AuthController> logger) {
            db = context;
            _env = environment;
            _configuration = configuration;
            _logger = logger;


        }

        [HttpPost]
        [Route("login")]
        public IActionResult Login(DataLogin dataLogin) {

            
             Users userLogin = db.Users.Where(x=>x.Email == dataLogin.Email).FirstOrDefault();

             var emailLogin = "";
             var emailAccount = "";

             if (dataLogin != null)
             {
                 try
                 {
                     emailLogin = prepareToCompareString(userLogin.Email);
                     emailAccount = prepareToCompareString(dataLogin.Email);
                    //Verifica el usuario y contraseña
                   string pass = BC.HashPassword(dataLogin.Password);
                    bool verified = BCrypt.Net.BCrypt.Verify(dataLogin.Password, userLogin.Password);

                    if (emailLogin != emailAccount || !BC.Verify(dataLogin.Password, userLogin.Password))
                    {
                        return Ok(new { 
                             response="Usuario/Contraseña incorrectos",
                             status=0
                         });
                     }
                     else
                     {

                         //Genera el token
                         var token = GenerarToken(userLogin);

                         return Ok(new
                         {
                             response = new JwtSecurityTokenHandler().WriteToken(token),
                             status=1
                         });;
                     }

                 }
                 catch (Exception e)
                 {
                     _logger.LogError("Login: " + e.Message, e);
                     return StatusCode((int)System.Net.HttpStatusCode.InternalServerError, e.Message);
                 }
             }
             else
             {
                 _logger.LogError("Login: El email y/o contraseña son incorrectos ");

                 return StatusCode((int)System.Net.HttpStatusCode.InternalServerError, "Login: El email y/o contraseña son incorrectos ");

             }
        }
        
        
        private JwtSecurityToken GenerarToken(Users login)
        {
            string ValidIssuer = _configuration["ApiAuth:Issuer"];
            string ValidAudience = _configuration["ApiAuth:Audience"];
            SymmetricSecurityKey IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["ApiAuth:SecretKey"]));
            var Account = db.Users.Where(x => x.Email == login.Email).FirstOrDefault();
            //La fecha de expiracion sera el mismo dia a las 12 de la noche
            DateTime dtFechaExpiraToken;
            DateTime now = DateTime.Now;
            dtFechaExpiraToken = new DateTime(now.Year, now.Month, now.Day, 23, 59, 59, 999);

            //Agregamos los claim nuestros
            List<Claim> claims = new List<Claim>
            {
                new Claim("NameIdentifier", Account.IdUser.ToString()),
                new Claim("Email", Account.Email),
                new Claim("Name", Account.DocumentNumber),

            };
            return new JwtSecurityToken
            (
                issuer: ValidIssuer,
                audience: ValidAudience,
                claims: claims,
                expires: dtFechaExpiraToken,
                notBefore: now,
                signingCredentials: new SigningCredentials(IssuerSigningKey, SecurityAlgorithms.HmacSha256)
            );
        }

        private string prepareToCompareString(string s)
        {
            Regex replace_a_Accents = new Regex("[á|à|ä|â]", RegexOptions.Compiled);
            Regex replace_e_Accents = new Regex("[é|è|ë|ê]", RegexOptions.Compiled);
            Regex replace_i_Accents = new Regex("[í|ì|ï|î]", RegexOptions.Compiled);
            Regex replace_o_Accents = new Regex("[ó|ò|ö|ô]", RegexOptions.Compiled);
            Regex replace_u_Accents = new Regex("[ú|ù|ü|û]", RegexOptions.Compiled);
            s = replace_a_Accents.Replace(s, "a");
            s = replace_e_Accents.Replace(s, "e");
            s = replace_i_Accents.Replace(s, "i");
            s = replace_o_Accents.Replace(s, "o");
            s = replace_u_Accents.Replace(s, "u");
            s = s.ToLower().Replace(" ", "");
            return s;
        }
        [HttpPost]
        [Route("Register")]
        public ActionResult<Users> Register(Users data)
        {

            data.Password = BC.HashPassword(data.Password);
            data.Status = 1;
            data.IdRoles = 2;
            string emit;
            var account = db.Users.Where(x => x.Email == data.Email).FirstOrDefault();
            if (account == null)
            {
                db.Users.Add(data);
                try
                {
                    db.SaveChanges();
                    //UtilController.EmailNewUser(data);
                    return Ok(new
                    {
                        response = "Cuenta registrada",
                        status = 1
                    }); 

                }
                catch
                {
                    emit = "No hemos podido registrar la cuenta.";
                    return Ok(new
                    {
                        response = emit,
                        status = 3
                    });
                    throw;
                }
            }
            else
            {
                emit = "La cuenta ya se encuentra registrada bajo los datos suministrados";
                return Ok(new
                {
                    response = emit,
                    status = 0
                });
            }

            //return CreatedAtAction("Login", new { Login }, Login);
        }
        [HttpGet]
        [Route("GetCurrentUser")]
        public async Task<ActionResult<Users>> GetCurrentUser()
        {

            var userId = this.User.FindFirstValue(ClaimTypes.NameIdentifier);
            var Email = this.User.FindFirstValue(ClaimTypes.Email);
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            Users account = new Users();
            Users getUser = new Users();
            if (identity != null)
            {
                IEnumerable<Claim> claims = identity.Claims;
                // or
                foreach (var item in claims)
                {
                    if (item.Type == "NameIdentifier")
                    {
                        account = await db.Users.Where(x => x.IdUser == int.Parse(item.Value)).FirstAsync();

                        
                        return account;

                    }
                }

            }
            else
            {
                return Ok(new
                {
                    response = "NO Existe data"
                });
            }
            return NoContent();
        }

    }
}
