﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication2.Data;
using WebApplication2.Models;
using WebApplication2.Models.Transaction;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebApplication2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BooksController : ControllerBase
    {
        private WebApplication2Context db;
        IHostingEnvironment _env;
       
        public BooksController(Data.WebApplication2Context context, IHostingEnvironment environment )
        {
            db = context;
            _env = environment;
             


        }
        // GET: api/<BooksController>
        [HttpGet]        
        public async Task <ActionResult<IEnumerable<GetBooks>>> Get()
        {
            // List<Books> books = await db.books.ToListAsync();
            List<GetBooks> GetBooks = new List<GetBooks>();

            GetBooks = await (
                from booksx in db.books
                join bookGenre in db.bookGenres on booksx.IdBookGenre equals bookGenre.IdBookGenre
                join editorials in db.editorials on booksx.IdEditorials equals editorials.IdEditorials
                select new GetBooks() { 
                    IdBooks = booksx.IdBooks,
                    Name = booksx.Name,
                    synapse = booksx.synapse,
                    NumberPages = booksx.NumberPages,
                    PublicationDate = booksx.PublicationDate,
                    NameEditorial = editorials.Name,
                    NameGenre = booksx.Name
                }
                ).ToListAsync();
            return GetBooks;
        }

        // GET api/<BooksController>/5
        [HttpGet("{id}")]
        public  async Task <ActionResult<GetBooks>> Get(int id)
        {
            Books book =  db.books.First();
            var GetBooks = await  (
                           from booksx in db.books
                           join bookGenre in db.bookGenres on booksx.IdBookGenre equals bookGenre.IdBookGenre
                           join editorials in db.editorials on booksx.IdEditorials equals editorials.IdEditorials
                           where booksx.IdBooks == id
                           select  new GetBooks()
                           {
                               IdBooks = booksx.IdBooks,
                               Name = booksx.Name,
                               synapse = booksx.synapse,
                               NumberPages = booksx.NumberPages,
                               PublicationDate = booksx.PublicationDate,
                               NameEditorial = editorials.Name,
                               NameGenre = booksx.Name
                           }
                           ).FirstAsync();
            return GetBooks;
        }

        // POST api/<BooksController>
        [HttpPost]
        [Route("CreateBook")]
        public ActionResult<Books> CreateBook(Books data)
        {

            string emit;
            if (data != null)
            {
                db.books.Add(data);
                try
                {
                    db.SaveChanges();
                    //UtilController.EmailNewUser(data);
                    return Ok(new
                    {
                        response = "Libro registrado",
                        status = 1
                    });

                }
                catch
                {
                    emit = "No hemos podido registrar la cuenta.";
                    return Ok(new
                    {
                        response = emit,
                        status = 3
                    });
                    throw;
                }
            }
            else
            {
                emit = "No se ha podido registrar el libro";
                return Ok(new
                {
                    response = emit,
                    status = 0
                });
            }

            //return CreatedAtAction("Login", new { Login }, Login);
        }



        // PUT api/<BooksController>/5
        [HttpPut("{id}")]
        [Authorize]
        public async Task<IActionResult> Put(int id, Books book)
        {
            if (id != id)
            {
                return BadRequest();
            }
            var boosk = db.books.Where(x => x.IdBooks == id).FirstOrDefault();
            db.Entry(book).State = EntityState.Modified;
            try
            {
                await db.SaveChangesAsync();

            }
            catch (DbUpdateConcurrencyException)
            {
                if (boosk == null)
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return Ok(new
            {
                response = "Se ha actualizado el libro",
                status = 1
            });

        }

        // DELETE api/<BooksController>/5
        [HttpDelete("{id}")]
        [Authorize]

        public async Task <ActionResult<Books>> Delete(int id)
        {

            Books book = await db.books.FindAsync(id);
           // List<Books> books2 = await db.books.ToListAsync();

            db.books.Remove(book);
            //books2.Remove(book);

            db.SaveChanges();
            return Ok(new
            {
                response = "Se ha eliminado el resgistro",
                status = 1
            });
        }
    }
}
