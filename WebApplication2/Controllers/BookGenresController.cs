﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication2.Data;
using WebApplication2.Models;

namespace WebApplication2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BookGenresController : ControllerBase
    {
        private WebApplication2Context db;
        IHostingEnvironment _env;

        public BookGenresController(Data.WebApplication2Context context, IHostingEnvironment environment)
        {
            db = context;
            _env = environment;



        }
        [HttpGet]
        public async Task<ActionResult<IEnumerable<BookGenre>>> Get()
        {

            List<BookGenre> bookGenres = await db.bookGenres.ToListAsync();
            return bookGenres;
        }
    }
}
