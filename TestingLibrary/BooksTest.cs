﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using WebApplication2.Controllers;
using WebApplication2.Data;
using WebApplication2.Models;

namespace TestingLibrary
{
    [TestClass]
    class BooksTest
    {
        private readonly BooksController _bookscrontroller;
        private WebApplication2Context db;
        IHostingEnvironment _env;
        
        public BooksTest(WebApplication2Context context, IHostingEnvironment environment)
        {
            db = context;
            _env = environment;
        }
        [TestMethod]
        public void CreateBookTesting(Books data) {
            BooksController CreateBook =  new BooksController(db,_env);
            Books Data = (new Books { 
                Name = "El lobo",
                synapse = "",
                NumberPages=23,
                PublicationDate=DateTime.Now,
                IdBookGenre=1,
                IdEditorials = 1
            });
           var Result =  CreateBook.CreateBook(Data);
            Assert.AreEqual(Result, Result);
        }
    }
}
